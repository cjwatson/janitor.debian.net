#!/usr/bin/python3

from aiohttp import web

from janitor.config import get_campaign_config
from janitor.site.common import render_template_for_request
from . import local_env
from yarl import URL


async def handle_apt_repo(target_release, request):
    suite = target_release + "-backports"
    from janitor.site.apt_repo import get_published_packages

    async with request.app.database.acquire() as conn:
        vs = {
            "packages": await get_published_packages(conn, suite),
            "suite": suite,
            "URL": URL,
            "target_release": target_release,
            "campaign_config": get_campaign_config(request.app['config'], suite),
        }
    text = await render_template_for_request(
        local_env, "backports/start.html", request, vs)
    return web.Response(
        content_type="text/html",
        text=text,
        headers={"Cache-Control": "max-age=60", "Vary": "Cookie"})


def register_backports_endpoints(router, target_release):
    router.add_get(
        "/%s-backports/" % target_release,
        lambda r: handle_apt_repo(target_release, r),
        name="%s-backports-start" % target_release
    )
