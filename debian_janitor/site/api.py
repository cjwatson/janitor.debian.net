#!/usr/bin/python3

from janitor import state
from janitor.config import get_campaign_config
from janitor.schedule import PolicyUnavailable, do_schedule

from aiohttp import web

from aiohttp_apispec import (
    docs,
    )

routes = web.RouteTableDef()


@docs()
@routes.get("/pkg", name="package-list")
@routes.get("/pkg/{package}", name="package")
async def handle_package_list(request):
    name = request.match_info.get("package")
    response_obj = []
    async with request.app['db'].acquire() as conn:
        query = 'SELECT name, maintainer_email, branch_url FROM package WHERE NOT removed'
        args = []
        if name:
            query += ' AND name = $1'
            args.append(name)
        for row in await conn.fetch(query, *args):
            response_obj.append(
                {
                    "name": row['name'],
                    "maintainer_email": row['maintainer_email'],
                    "branch_url": row['branch_url'],
                }
            )
    return web.json_response(response_obj, headers={"Cache-Control": "max-age=600"})


@docs()
@routes.get("/pkg/{package}/run/{run_id}", name="package-run")
@routes.get("/run/{run_id}", name="run")
async def handle_run(request):
    run_id = request.match_info.get("run_id")
    async with request.app['db'].acquire() as conn:
        run = await conn.fetchrow(
            'SELECT id, start_time, finish_time, command, description, '
            'package, build_info, result_code, vcs_type, branch_url '
            'debian_build.version as build_version, '
            'debian_build.distribution as build_distribution '
            'FROM run LEFT JOIN debian_build ON debian_build.run_id = run.id '
            'WHERE id = $1', run_id)
    if run is None:
        raise web.HTTPNotFound(text='no such run')
    if run['build_version']:
        build_info = {
            "version": str(run['build_version']),
            "distribution": run['build_distribution'],
        }
    else:
        build_info = None
    return web.json_response({
            "run_id": run['id'],
            "start_time": run['start_time'].isoformat(),
            "finish_time": run['finish_time'].isoformat(),
            "command": run['command'],
            "description": run['description'],
            "package": run['package'],
            "build_info": build_info,
            "result_code": run['result_code'],
            "vcs_type": run['vcs_type'],
            "branch_url": run['branch_url'],
        }, headers={"Cache-Control": "max-age=600"})


@docs()
@routes.get("/policy", name="policy")
async def handle_global_policy(request):
    return web.Response(
        content_type="text/protobuf",
        text=str(request.app['policy']),
        headers={"Cache-Control": "max-age=60"},
    )


@docs()
@routes.post("/vcswatch", name="vcswatch")
async def handle_vcswatch(request):
    json = await request.json()
    # Keys set:
    # * old-hash
    # * new-hash
    # * package
    # * status
    # * branch
    # * url

    package = json['package']
    url = json['url']

    rescheduled = []
    policy_unavailable = []
    requestor = "vcwatch notification"
    async with request.app['db'].acquire() as conn:
        if await state.has_cotenants(conn, package, url):
            # TODO(jelmer): Have vcswatch pass along path, and only
            # notify for changes under path
            return web.json_response({
                'rescheduled': [],
                'policy-unavailable': [],
                'ignored': 'package is in repository with cotenants',
                }, status=200)
        for suite in await state.iter_publishable_suites(conn, package):
            try:
                campaign = get_campaign_config(request.app['config'], suite)
            except KeyError:
                continue
            if not campaign.webhook_trigger:
                continue
            try:
                await do_schedule(
                    conn, package, suite, requestor=requestor, bucket="hook")
            except PolicyUnavailable:
                policy_unavailable.append(suite)
            else:
                rescheduled.append(suite)

    return web.json_response({
        'rescheduled': rescheduled,
        'policy-unavailable': policy_unavailable,
        }, status=200)


@docs()
@routes.get("/{suite}/published-packages", name="published-packages")
async def handle_published_packages(request):
    from janitor.site.apt_repo import get_published_packages
    suite = request.match_info["suite"]
    async with request.app['db'].acquire() as conn:
        response_obj = []
        for (
            package,
            build_version,
            archive_version,
        ) in await get_published_packages(conn, suite):
            response_obj.append(
                {
                    "package": package,
                    "build_version": build_version,
                    "archive_version": archive_version,
                }
            )
    return web.json_response(response_obj)
