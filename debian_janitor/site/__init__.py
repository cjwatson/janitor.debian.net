#!/usr/bin/python
# Copyright (C) 2019 Jelmer Vernooij <jelmer@jelmer.uk>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

from jinja2 import FileSystemLoader, select_autoescape, Environment
import os
from yarl import URL

from janitor.site import (
    __file__ as _common_site_path,
    format_duration,
    format_timestamp,
    highlight_diff,
    classify_result_code,
    )

local_env = Environment(
    loader=FileSystemLoader([
        os.path.join(os.path.dirname(__file__), 'templates'),
        os.path.join(os.path.dirname(_common_site_path), 'templates')]),
    autoescape=select_autoescape(["html", "xml"]),
    enable_async=True,
)
local_env.globals.update(format_duration=format_duration)
local_env.globals.update(format_timestamp=format_timestamp)
local_env.globals.update(enumerate=enumerate)
local_env.globals.update(highlight_diff=highlight_diff)
local_env.globals.update(classify_result_code=classify_result_code)
local_env.globals.update(URL=URL)
