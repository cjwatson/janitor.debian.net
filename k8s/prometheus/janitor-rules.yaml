apiVersion: monitoring.coreos.com/v1
kind: PrometheusRule
metadata:
  creationTimestamp: null
  labels:
    prometheus: k8s
    role: alert-rules
  name: janitor-rules
spec:
  groups:
  - name: janitor rules
    rules:
    - alert: JanitorRunnerDown
      expr: up{job="runner"} < 1
      for: 1h
      labels:
        severity: critical
        project: debian-janitor
      annotations:
        description: Job {{ $labels.job }} instance {{ $labels.instance }} is down.
        summary: Janitor runner is down
    - alert: JanitorPublisherDown
      expr: up{job="publish"} < 1
      for: 1h
      labels:
        severity: critical
        project: debian-janitor
      annotations:
        description: Job {{ $labels.job}} instance {{ $labels.instance }} is down.
        summary: Janitor publisher is down
    - alert: JanitorSiteDown
      expr: up{job="site"} < 1
      for: 1h
      labels:
        severity: critical
        project: debian-janitor
      annotations:
        description: Job {{ $labels.job}} instance {{ $labels.instance }} is down.
        summary: Janitor site is down
    - alert: JanitorDifferDown
      expr: up{job="differ"} < 1
      for: 1h
      labels:
        severity: critical
        project: debian-janitor
      annotations:
        description: Job {{ $labels.job }} instance {{ $labels.instance }} is down.
        summary: Janitor differ is down
    - alert: JanitorArchiverDown
      expr: up{job="archive"} < 1
      for: 1h
      labels:
        severity: critical
        project: debian-janitor
      annotations:
        description: Job {{ $labels.job}} instance {{ $labels.instance }} is down.
        summary: Janitor archiver is down
    - alert: JanitorVcsStoreDown
      expr: up{job="vcs-store"} < 1
      for: 1h
      labels:
        severity: critical
        project: debian-janitor
      annotations:
        description: Job {{ $labels.job}} instance {{ $labels.instance }} is down.
        summary: Janitor vcs store is down
    - alert: JanitorScheduleNotRunning
      expr: ((time() - job_last_success_unixtime{exported_job="janitor.schedule"}) / 3600) > 48
      for: 1h
      annotations:
        summary: janitor.schedule not running
      labels:
        project: debian-janitor
    - alert: JanitorIrcRelayDown
      expr: up{job="irc-alertmanager"} < 1
      for: 1h
      labels:
        severity: critical
        project: debian-janitor
      annotations:
        description: Job {{ $labels.job}} instance {{ $labels.instance }} is down.
        summary: Prometheus IRC relay is down
    - alert: JanitorIrcNotifyDown
      expr: up{job="irc_notify"} < 1
      for: 1h
      labels:
        severity: critical
        project: debian-janitor
      annotations:
        description: Job {{ $labels.job}} instance {{ $labels.instance }} is down.
        summary: Janitor IRC notifier is down
    - alert: JanitorMastodonNotifyDown
      expr: up{job="mastodon-notify"} < 1
      for: 1h
      labels:
        severity: critical
        project: debian-janitor
      annotations:
        description: Job {{ $labels.job }} instance {{ $labels.instance }} is down.
        summary: Janitor Mastodon notifier is down
    - alert: JanitorRunFinishFailing
      expr: increase(request_exceptions_total{job="runner",route="finish"}[1h]) > 0
      for: 5m
      labels:
        severity: critical
        project: debian-janitor
      annotations:
        description: Finish requests to the runner {{ $labels.instance }} are failing.
        summary: Run finish requests are failing

    - alert: JanitorRunAssignFailing
      expr: increase(request_exceptions_total{job="runner",route="assign"}[1h]) > 0
      for: 5m
      labels:
        severity: critical
        project: debian-janitor
      annotations:
        description: "Assign requests to the runner {{ $labels.instance }} are failing."
        summary: Run assign requests are failing

#  - alert: JanitorHighRequestFailureRate
#    expr: increase(request_exceptions_total[30m]) > 5
#    for: 0m
#    labels:
#      severity: critical
#      project: debian-janitor
#    annotations:
#      description: "{{ $labels.route }} requests to {{ $labels.instance }} are failing."
#      summary: {{ $labels.route }} requests are failing
#
#  - alert: JanitorRequestFailures
#    expr: increase(request_exceptions_total[30m]) > 0
#    for: 0m
#    labels:
#      severity: warning
#      project: debian-janitor
#    annotations:
#      description: "{{ $labels.route }} requests to {{ $labels.instance }} are failing."
#      summary: {{ $labels.route }} requests are failing

    - alert: JobRestarting
      expr: avg without(instance)(changes(process_start_time_seconds[1h])) > 3
      for: 10m
      labels:
        severity: critical
        project: debian-janitor
      annotations:
        summary: "{{ $labels.job }} is repeatedly restarting (>3 times in the last hour)"
        description: "{{ $labels.job }} is repeatedly restarting (>3 times in the last hour)"

    - alert: SiteDown
      expr: probe_http_status_code{job="blackbox_https"} != 200
      for: 5m
      labels:
        severity: critical
        project: debian-janitor

    - alert: SuccessRateTooLow
      expr: sum(sum_over_time(result_total{result_code=~"(success|nothing-to-do)"}[1h])) / sum(sum_over_time(result_total[1h])) < 0.1
      for: 30m
      labels:
        severity: warning
        project: debian-janitor
      annotations:
        description: Success rate is too low
        summary: Success rate is too low
    - alert: NoAssignments
      expr: sum(increase(assignments_total{worker!="blodeuwedd",worker!="charis"}[1h])) by (worker) < 10
      for: 30m
      labels:
        severity: warning
        project: debian-janitor
      annotations:
        description: "No assignments are being requested or handed out on workers {{ $labels.worker }}"
        summary: "Worker {{ $labels.worker }} are not requesting assignments"
    - alert: CommonFailure
      expr: sum(sum_over_time(result_total[1h])) > 10 and topk(1, sum by(result_code) (result_total{result_code!~"(success|nothing-to-do)"})) > .1 * sum(result_total)
      for: 30m
      labels:
        severity: warning
        project: debian-janitor
      annotations:
        description: Many runs are failing with the same code
        summary: Many runs failing
    - alert: ArchivePublishFailing
      expr: time() - last_suite_publish_success > 60 * 60 * 12
      for: 30m
      labels:
        severity: warning
        project: debian-janitor
      annotations:
        summary: "archive publishing for {{ $labels.suite }} has been failing >12h"
        description: "Archive publishing for suite {{ $labels.suite }} has been failing for more than twelve hours"
    - alert: PublishPendingFailing
      expr: time() - last_publish_pending_success > 60 * 60 * 24
      for: 4h
      labels:
        severity: warning
        project: debian-janitor
      annotations:
        summary: publish pending has been failing for >24h
        description: pending change publishing has been failing for more than a day
