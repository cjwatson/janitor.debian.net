This repository contains the configuration for https://janitor.debian.net/.
It's an instance of the Janitor project, which can be found at
https://salsa.debian.org/jelmer/debian-janitor.

schedule/ contains the various scripts for importing package metadata
and candidates.

See the k8s/ directory for the kubernetes configuration.

If you're looking to adjust the policy for a specific team, package, maintainer
or URL please take a look at the [policy.conf](k8s/policy.conf) file.

Contributing
============

The easiest way to get started with contributing to the Janitor is to work on
identifying issues and adding fixers for lintian-brush. There is
[a guide](https://salsa.debian.org/jelmer/lintian-brush/-/blob/master/doc/fixer-writing-guide.rst)
on identifying good candidates and writing fixers in the lintian-brush
repository.

If you're interested in working on adding another campaign, see
[adding-a-new-campaign](https://github.com/jelmer/janitor/blob/master/devnotes/adding-a-new-campaign.rst).
