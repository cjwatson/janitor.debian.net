#!/usr/bin/python3

# Copyright (C) 2018-2020 Jelmer Vernooij <jelmer@jelmer.uk>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

from janitor.candidates_pb2 import Candidate, CandidateList
from debian_janitor.udd import DEFAULT_UDD_URL


DEFAULT_VALUE = 50


async def iter_watch_fixes_candidates(udd, packages):
    args = []
    query = """
SELECT source, version, errors, warnings, last_check FROM
upstream_status
WHERE
  status = 'error' AND release = 'sid'
"""
    if packages is not None:
        query += " AND source = any($2::text[])"
        args.append(tuple(packages))
    query += " ORDER BY source, version DESC"
    for row in await udd.fetch(query, *args):
        candidate = Candidate()
        candidate.package = row['source']
        candidate.context = row['last_check'].isoformat()
        candidate.value = DEFAULT_VALUE
        candidate.suite = "watch-fixes"
        yield candidate


async def main():
    import argparse
    import asyncpg

    parser = argparse.ArgumentParser(prog="watch-fixes-candidates")
    parser.add_argument("packages", nargs="*", default=None)
    parser.add_argument("--udd-url", type=str, default=DEFAULT_UDD_URL, help="UDD URL")

    args = parser.parse_args()

    udd = await asyncpg.connect(args.udd_url)
    async for candidate in iter_watch_fixes_candidates(
        udd, args.packages or None,
    ):
        cl = CandidateList()
        cl.candidate.append(candidate)
        print(cl)


if __name__ == "__main__":
    import asyncio

    asyncio.run(main())
