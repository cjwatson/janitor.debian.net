#!/usr/bin/python3

import argparse
from debian.deb822 import PkgRelation
import logging
import psycopg2
import re
import sys
import yaml

from debian_janitor.udd import DEFAULT_UDD_URL


parser = argparse.ArgumentParser()

parser = argparse.ArgumentParser(prog="candidates")
parser.add_argument("--gcp-logging", action='store_true', help='Use Google cloud logging.')
parser.add_argument("--release", type=str, default="sid")
parser.add_argument('--udd-url', type=str, help='UDD URL', default=DEFAULT_UDD_URL)
args = parser.parse_args()

if args.gcp_logging:
    import google.cloud.logging
    client = google.cloud.logging.Client()
    client.get_default_handler()
    client.setup_logging()
else:
    logging.basicConfig(level=logging.INFO)

conn = psycopg2.connect(args.udd_url)
conn.set_client_encoding('UTF8')
cur = conn.cursor()
cur.execute("""\
SELECT package, description, depends FROM packages WHERE release = %s AND description LIKE '%%transitional%%'""", (args.release, ))

transition = []

REGEXES = [
    r'.*\((.*, )?(dummy )?transitional (dummy )?package\)',
    r'.*\((.*, )?(dummy )?transitional (dummy )?package for ([^ ]+)\)',
    r'.*\(transitional development files\)',
    r'.*\(transitional\)',
    r'.* [-—] transitional( package)?',
    r'.*\[transitional package\]',
    r'.* - transitional (dummy )?package',
    r'transitional package -- safe to remove',
    r'(dummy )?transitional (dummy )?package (for|to) (.*)',
    r'transitional dummy package',
    r'transitional dummy package: ([^ ]+)',
    r'transitional package, ([^ ]+)',
    r'(dummy )?transitional (dummy )?package, ([^ ]+) to ([^ ]+)',
    r'transitional package( [^ ]+)?',
    r'([^ ]+) transitional package',
    r'.* transitional package',
    r'.*transitional package for .*',
    ]

for row in cur.fetchall():
    for regex in REGEXES:
        if re.fullmatch(regex, row[1]):
            break
    else:
        logging.warning(
            'Unknown syntax for dummy package description: %r', row[1])
    if row[2] is None:
        logging.warning('no replacement for %s', row[0])
    else:
        depends = PkgRelation.parse_relations(row[2])
        if len(depends) != 1:
            logging.warning('no single transition target for %s: %r', row[0], row[2])
    transition.append({
        'from_name': row[0],
        'to_expr': row[2]
    })

yaml.dump(transition, sys.stdout)
