#!/bin/bash -ex
SCHEDULE=${SCHEDULE:-/schedule}
CODE=${CODE:-/code}
EXTRA_UPSTREAM_PROJECTS=${EXTRA_UPSTREAM_PROJECTS:-/etc/janitor/extra_upstream_projects.conf}
CONFIG=${CONFIG:-/etc/janitor/janitor.conf}
PACKAGE_OVERRIDES=${PACKAGE_OVERRIDES:-/etc/janitor/package_overrides.conf}
POLICY=${POLICY:-/etc/janitor/policy.conf}
LOGGING=${LOGGING:---gcp-logging}
export PYTHONPATH="${SCHEDULE}:${CODE}:${CODE}/lintian-brush:${CODE}/silver-platter:${CODE}/breezy:${CODE}/dulwich:${CODE}/debmutate:${CODE}/upstream-ontologist"
${SCHEDULE}/upstream-codebases.py --extra-upstream-projects=${EXTRA_UPSTREAM_PROJECTS} | python3 -m janitor.package_metadata --distribution=upstream --config=${CONFIG} ${LOGGING} "$@"
${SCHEDULE}/udd-package-metadata.py --package-overrides=${PACKAGE_OVERRIDES} ${LOGGING} | python3 -m janitor.package_metadata --distribution=unstable --config=${CONFIG} ${LOGGING} "$@"
python3 -m debian_janitor.policy --config=${CONFIG} --policy=${POLICY} ${LOGGING}
(
   python3 ${SCHEDULE}/unchanged-candidates.py
   python3 ${SCHEDULE}/backports-candidates.py --target-release=bullseye
   python3 ${SCHEDULE}/backports-candidates.py --target-release=buster
   python3 ${SCHEDULE}/upstream-unchanged-candidates.py --config=${CONFIG}
   python3 ${SCHEDULE}/scrub-obsolete-candidates.py
   python3 ${SCHEDULE}/lintian-fixes-candidates.py
   python3 ${SCHEDULE}/fresh-releases-candidates.py
   python3 ${SCHEDULE}/fresh-snapshots-candidates.py
   python3 ${SCHEDULE}/multi-arch-candidates.py
   python3 ${SCHEDULE}/orphan-candidates.py
   python3 ${SCHEDULE}/mia-candidates.py
   python3 ${SCHEDULE}/uncommitted-candidates.py
   python3 ${SCHEDULE}/debianize-candidates.py --config=${CONFIG}
   python3 ${SCHEDULE}/watch-fixes-candidates.py
) | python3 -m janitor.candidates --config=${CONFIG} ${LOGGING} "$@"
python3 -m janitor.schedule --config=${CONFIG} ${LOGGING} "$@"
# Just gather transitions for now, but don't use them
(
  python3 ${SCHEDULE}/transitions_packages.py ${LOGGING}
) >/dev/null
