#!/usr/bin/python3

from janitor.candidates_pb2 import Candidate, CandidateList
from debian_janitor.udd import DEFAULT_UDD_URL


DEFAULT_VALUE_ORPHAN = 60


async def iter_orphan_candidates(udd, packages=None):
    args = []
    query = """\
SELECT DISTINCT ON (sources.source) sources.source, now() - orphaned_time, bug
FROM sources
JOIN orphaned_packages ON orphaned_packages.source = sources.source
WHERE sources.vcs_url != '' AND sources.release = 'sid' AND
orphaned_packages.type in ('O') AND
(sources.uploaders != '' OR
sources.maintainer != 'Debian QA Group <packages@qa.debian.org>')
"""
    if packages is not None:
        query += " AND sources.source = any($1::text[])"
        args.append(tuple(packages))
    for row in await udd.fetch(query, *args):
        candidate = Candidate()
        candidate.package = row[0]
        candidate.suite = "orphan"
        candidate.context = str(row[2])
        candidate.value = DEFAULT_VALUE_ORPHAN
        yield candidate


async def main():
    import argparse
    import asyncpg

    parser = argparse.ArgumentParser(prog="orphan-candidates")
    parser.add_argument("packages", nargs="*", default=None)
    parser.add_argument("--udd-url", type=str, default=DEFAULT_UDD_URL, help="UDD URL")

    args = parser.parse_args()

    udd = await asyncpg.connect(args.udd_url)
    async for candidate in iter_orphan_candidates(udd, args.packages or None):
        cl = CandidateList()
        cl.candidate.append(candidate)
        print(cl)


if __name__ == "__main__":
    import asyncio

    asyncio.run(main())
