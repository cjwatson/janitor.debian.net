#!/usr/bin/python3
# Copyright (C) 2018 Jelmer Vernooij <jelmer@jelmer.uk>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

"""Exporting of upstream metadata from UDD."""

from email.utils import parseaddr
import itertools
import logging
import re

from debian.changelog import Version
from debmutate.vcs import unsplit_vcs_url, split_vcs_url
from typing import List, Optional, Iterator, AsyncIterator, Tuple
import urllib.parse

from breezy import urlutils

from lintian_brush.salsa import (
    guess_repository_url,
    salsa_url_from_alioth_url,
)
from lintian_brush.vcs import (
    determine_browser_url,
    )

from janitor.package_metadata_pb2 import PackageList
from janitor.vcs import (
    open_branch_ext,
    BranchOpenFailure,
    get_vcs_abbreviation,
)


from debian_janitor.udd import DEFAULT_UDD_URL
from silver_platter.debian import (
    select_preferred_probers,
    select_probers,
    )


def extract_uploader_emails(uploaders: str) -> List[str]:
    if not uploaders:
        return []
    ret = []
    for uploader in uploaders.split(","):
        if not uploader:
            continue
        email = parseaddr(uploader)[1]
        if not email:
            continue
        ret.append(email)
    return ret


def is_alioth_url(url: str) -> bool:
    return urllib.parse.urlparse(url).netloc in (
        "svn.debian.org",
        "bzr.debian.org",
        "anonscm.debian.org",
        "hg.debian.org",
        "git.debian.org",
        "alioth.debian.org",
    )


async def iter_packages_with_metadata(
    udd, packages: Optional[List[str]] = None
) -> AsyncIterator[
    Tuple[
        str,
        str,
        str,
        int,
        str,
        str,
        str,
        str,
        str,
        str,
        str,
        str,
        str,
        Version,
        Version,
    ]
]:
    args = []
    query = """
select distinct on (sources.source) sources.source,
sources.maintainer_email, sources.uploaders, popcon_src.insts,
vcswatch.vcs, sources.vcs_type,
vcswatch.url, sources.vcs_url,
vcswatch.branch,
vcswatch.browser, sources.vcs_browser,
commit_id,
status as vcswatch_status,
sources.version,
vcswatch.version,
sources.original_maintainer
from sources left join popcon_src on sources.source = popcon_src.source
left join vcswatch on vcswatch.source = sources.source
where sources.release = 'sid'
"""
    if packages:
        query += " and sources.source = ANY($1::text[])"
        args.append(packages)
    query += " order by sources.source, sources.version desc"
    for row in await udd.fetch(query, *args):
        yield row


async def iter_removals(udd, packages: Optional[List[str]] = None) -> Iterator:
    query = """\
select name, version from package_removal where 'source' = any(arch_array)
"""
    args = []
    if packages:
        query += " and name = ANY($1::text[])"
        args.append(packages)
    return await udd.fetch(query, *args)


def possible_urls_from_alioth_url(vcs_type, vcs_url):
    # These are the same transformations applied by vcswatc. The goal is mostly
    # to get a URL that properly redirects.
    https_alioth_url = re.sub(
        r"(https?|git)://(anonscm|git).debian.org/(git/)?",
        r"https://anonscm.debian.org/git/",
        vcs_url,
    )

    yield https_alioth_url
    yield salsa_url_from_alioth_url(vcs_type, vcs_url)


def possible_salsa_urls_from_package_name(package_name, maintainer_email=None):
    yield guess_repository_url(package_name, maintainer_email)
    yield "https://salsa.debian.org/debian/%s.git" % package_name


def open_guessed_salsa_branch(
    pkg, maintainer_email, vcs_type, vcs_url, possible_transports=None,
):
    probers = select_probers("git")
    vcs_url, params = urlutils.split_segment_parameters_raw(vcs_url)

    tried = set(vcs_url)

    for salsa_url in itertools.chain(
        possible_urls_from_alioth_url(vcs_type, vcs_url),
        possible_salsa_urls_from_package_name(pkg, maintainer_email),
    ):
        if not salsa_url or salsa_url in tried:
            continue

        tried.add(salsa_url)

        salsa_url = urlutils.join_segment_parameters_raw(salsa_url, *params)

        logging.debug("Trying to access salsa URL %s instead.", salsa_url)
        try:
            branch = open_branch_ext(
                salsa_url,
                possible_transports=possible_transports, probers=probers)
        except BranchOpenFailure:
            pass
        else:
            logging.info("Converting alioth URL: %s -> %s", vcs_url, salsa_url)
            return branch
    return None


def find_alioth_replacement(package, maintainer_email, vcs_type, repo_url, branch_name=None, possible_transports=None):
    probers = select_preferred_probers(vcs_type)
    logging.info(
        'Opening branch %s with %r', repo_url,
        [p.__name__ for p in probers])
    try:
        return open_branch_ext(repo_url, possible_transports=possible_transports, probers=probers)
    except BranchOpenFailure:
        try:
            return open_guessed_salsa_branch(
                package,
                maintainer_email,
                vcs_type,
                repo_url,
                possible_transports=possible_transports,
            )
        except BranchOpenFailure:
            return None


async def main():
    possible_transports = []
    import argparse
    import asyncpg
    from debian_janitor.package_overrides import read_package_overrides

    parser = argparse.ArgumentParser(prog="candidates")
    parser.add_argument("packages", nargs="*")
    parser.add_argument("--gcp-logging", action='store_true', help='Use Google cloud logging.')
    parser.add_argument("--udd-url", type=str, default=DEFAULT_UDD_URL, help="UDD URL")
    parser.add_argument(
        "--package-overrides",
        type=str,
        help="Path to package overrides.",
        default=None,
    )
    args = parser.parse_args()

    if args.gcp_logging:
        import google.cloud.logging
        client = google.cloud.logging.Client()
        client.get_default_handler()
        client.setup_logging()
    else:
        logging.basicConfig(level=logging.INFO)

    if args.package_overrides:
        with open(args.package_overrides, "r") as f:
            package_overrides = read_package_overrides(f)
    else:
        package_overrides = {}

    udd = await asyncpg.connect(args.udd_url)

    removals = {}
    for name, version in await iter_removals(udd, packages=args.packages):
        if name not in removals:
            removals[name] = Version(version)
        else:
            removals[name] = max(Version(version), removals[name])

    for name, version in removals.items():
        pl = PackageList()
        removal = pl.removal.add()
        removal.name = name
        removal.version = str(version)
        print(pl)

    async for (
        name,
        maintainer_email,
        uploaders,
        insts,
        vcswatch_vcs_type,
        control_vcs_type,
        vcswatch_vcs_url,
        control_vcs_url,
        vcswatch_branch,
        vcswatch_vcs_browser,
        control_vcs_browser,
        commit_id,
        vcswatch_status,
        sid_version,
        vcswatch_version,
        original_maintainer
    ) in iter_packages_with_metadata(udd, args.packages):
        pl = PackageList()
        package = pl.package.add()
        package.name = name
        if maintainer_email is None:
            logging.warning('package %s has no maintainer set' % name)
        else:
            package.maintainer_email = maintainer_email
        package.uploader_email.extend(extract_uploader_emails(uploaders))
        package.in_base = (original_maintainer is not None)
        if insts is not None:
            package.insts = insts
        if vcswatch_vcs_type:
            package.vcs_type = vcswatch_vcs_type
            repo_url, oldbranch, subpath = split_vcs_url(vcswatch_vcs_url)
            if oldbranch != vcswatch_branch:
                package.vcs_url = unsplit_vcs_url(repo_url, vcswatch_branch, subpath)
                logging.info(
                    "Fixing up branch name from vcswatch: %s -> %s"
                    % (vcswatch_vcs_url, package.vcs_url)
                )
            else:
                package.vcs_url = vcswatch_vcs_url
            if vcswatch_vcs_browser:
                package.vcs_browser = vcswatch_vcs_browser
        elif control_vcs_type:
            package.vcs_type = control_vcs_type
            package.vcs_url = control_vcs_url
            if control_vcs_browser:
                package.vcs_browser = control_vcs_browser
        repo_url, branch_name, subpath = split_vcs_url(package.vcs_url)
        if is_alioth_url(repo_url):
            logging.info(
                'Attempting to find a replacement for obsolete alioth URL %s',
                repo_url)
            new_branch = find_alioth_replacement(
                name, maintainer_email, package.vcs_type, repo_url, branch_name, possible_transports=possible_transports)
            if new_branch:
                package.vcs_type = get_vcs_abbreviation(new_branch.repository)
                package.vcs_url = unsplit_vcs_url(new_branch.repository.user_url.rstrip('/'), new_branch.name, subpath)
                package.vcs_browser = determine_browser_url(package.vcs_type, package.vcs_url)
        if commit_id:
            package.commit_id = commit_id
        if vcswatch_status:
            package.vcswatch_status = vcswatch_status
        package.archive_version = sid_version
        if vcswatch_version:
            package.vcswatch_version = vcswatch_version
        if name not in removals:
            package.removed = False
        else:
            package.removed = Version(sid_version) <= removals[name]
        try:
            override = package_overrides[name]
        except KeyError:
            pass
        else:
            if override.branch_url:
                package.vcs_url = override.branch_url
                package.origin = args.package_overrides
        print(pl)


if __name__ == "__main__":
    import asyncio

    asyncio.run(main())
