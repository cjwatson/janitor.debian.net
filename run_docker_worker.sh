#!/bin/bash -e

docker run -e BUILD_URL="${BUILD_URL}" -p8080:8080 --cap-add=SYS_ADMIN --privileged -v ${DEBIAN_JANITOR_CREDENTIALS}:/credentials.json registry.salsa.debian.org/jelmer/janitor.debian.net/worker --credentials=/credentials.json --base-url https://janitor.debian.net/api/ "$@"
