CREATE OR REPLACE VIEW absorbed_multiarch_hints AS
  select package, id, x->>'binary' as binary, x->>'link'::text as link, x->>'severity' as severity, x->>'source' as source, (x->>'version')::debversion as version, x->'action' as action, x->>'certainty' as certainty from (select package, id, json_array_elements(result->'applied-hints') as x from absorbed_runs where suite = 'multiarch-fixes') as f;

CREATE OR REPLACE VIEW multiarch_hints AS
  select package, id, x->>'binary' as binary, x->>'link'::text as link, x->>'severity' as severity, x->>'source' as source, (x->>'version')::debversion as version, x->'action' as action, x->>'certainty' as certainty from (select package, id, json_array_elements(result->'applied-hints') as x from run where suite = 'multiarch-fixes') as f;
CREATE VIEW lintian_results AS
   select run_id, path, name, context, severity from debian_build, json_to_recordset(lintian_result->'groups'->0->'input-files') as entries(path text, tags json), json_to_recordset(tags) as hint(context text, name text, severity text);
CREATE OR REPLACE VIEW absorbed_lintian_fixes AS
  select absorbed_runs.*, x.summary, x.description as fix_description, x.certainty, x.fixed_lintian_tags from absorbed_runs, json_to_recordset((result->'applied')::json) as x("summary" text, "description" text, "certainty" text, "fixed_lintian_tags" text[]);

CREATE OR REPLACE VIEW last_unabsorbed_lintian_fixes AS
  select last_unabsorbed_runs.*, x.summary, x.description as fix_description, x.certainty, x.fixed_lintian_tags from last_unabsorbed_runs, json_to_recordset((result->'applied')::json) as x("summary" text, "description" text, "certainty" text, "fixed_lintian_tags" text[]) WHERE result_code = 'success';
